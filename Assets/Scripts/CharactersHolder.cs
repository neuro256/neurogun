﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun
{
    public class CharactersHolder 
    {
        // модификатор public установлен в качестве тестового 
        public List<Character> characters = new List<Character>();

        private void Character_OnHealthChanged(float currentHealthPercent)
        {

        }

        private void Character_OnCharacterDeactivate(string tag, string id)
        {

        }

        private void Character_OnCharacterActivate(string tag, string id)
        {

        }

        public void Add(Character pCharacter)
        {
            Debug.Log("Add character: " + pCharacter.characterInfo.name);
            characters.Add(pCharacter);
            pCharacter.onCharacterActivate += Character_OnCharacterActivate;
            pCharacter.onCharacterDeactivate += Character_OnCharacterDeactivate;
            pCharacter.onHealthChanged += Character_OnHealthChanged;
        }

        public void Remove(Character pCharacter)
        {
            Debug.Log("Remove character: " + pCharacter.characterInfo.name);

            pCharacter.onCharacterActivate -= Character_OnCharacterActivate;
            pCharacter.onCharacterDeactivate -= Character_OnCharacterDeactivate;
            pCharacter.onHealthChanged -= Character_OnHealthChanged;

            characters.Remove(pCharacter);
        }
    }
}
