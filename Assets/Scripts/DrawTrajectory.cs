﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun
{
    public class DrawTrajectory : MonoBehaviour
    {
        [SerializeField]
        private LineRenderer _lineRenderer;
        [SerializeField]
        [Range(3, 100)]
        private int _lineSegmentCount = 100;

        private List<Vector3> _linePoints;

        private void Awake()
        {
            _linePoints = new List<Vector3>();
        }

        public void UpdateTrajectory(Vector3 pForceVector, Rigidbody pRigibody, Vector3 pStartingPos)
        {
            Vector3 velocity = (pForceVector / pRigibody.mass) * Time.fixedDeltaTime;

            float flightDuration = (2 * velocity.y) / Physics.gravity.y;

            float stepTime = flightDuration / _lineSegmentCount;

            _linePoints.Clear();

            for (int i = 0; i < _lineSegmentCount; i++)
            {
                float stepTimePassed = stepTime * i;

                Vector3 movementVector = new Vector3(
                    velocity.x * stepTimePassed,
                    velocity.y * stepTimePassed - 0.5f * Physics.gravity.y * stepTimePassed * stepTimePassed,
                    velocity.z * stepTimePassed
                    );

                _linePoints.Add(-movementVector + pStartingPos);

                RaycastHit hit;
                if (Physics.Raycast(pStartingPos, -movementVector, out hit, movementVector.magnitude))
                {
                    break;
                }
            }

            _lineRenderer.positionCount = _linePoints.Count;
            _lineRenderer.SetPositions(_linePoints.ToArray());
        }

        public void HideLine()
        {
            _lineRenderer.positionCount = 0;
        }
    }
}
