﻿using Neurogun.Interfaces;
using Neurogun.UI;
using Neurogun.Weapons;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun
{
    [System.Serializable]
    public class CharacterInfo
    {
        [SerializeField]
        private string _tag;
        [SerializeField]
        private float _maxHealth;

        private string _id = null;
        private string _name;
        private float _currentHealth;
        private string _teamName;
        private Color _teamColor;

        public string tag { get => _tag; set => _tag = value; }
        public float maxHealth { get => _maxHealth; set => _maxHealth = value; }
        public float currentHealth
        {
            get => _currentHealth;
            set
            {
                _currentHealth = value;
                if (_currentHealth > _maxHealth)
                    _currentHealth = _maxHealth;
                else if (_currentHealth < 0)
                    _currentHealth = 0;
            }
        }
        public string teamName { get => _teamName; }
        public Color teamColor { get => _teamColor; }
        public string id { get => _id; set => _id = value; }
        public string name { get => _name; }

        public void SetTeamData(string pTeamName, Color pTeamColor)
        {
            _teamName = pTeamName;
            _teamColor = pTeamColor;
        }

        public void SetName(string pName)
        {
            _name = pName;
        }
    }

    public abstract class Character : MonoBehaviour, IDamageable
    {
        [SerializeField]
        private CharacterInfo _characterInfo = new CharacterInfo();

        private Color _healthbarColor;

        public CharacterInfo characterInfo { get => _characterInfo; }

        public event Action<float> onHealthChanged = delegate { };
        public event Action<string, string> onCharacterActivate = delegate { };
        public event Action<string, string> onCharacterDeactivate = delegate { };
        public event Action<string, string> onCharacterRespawn = delegate { };

        public Color healthbarColor { get => _healthbarColor; set => _healthbarColor = value; }

        protected void ModifyHealth(float pAmount)
        {
            _characterInfo.currentHealth -= pAmount;

            float currentHealthPercent = (float)_characterInfo.currentHealth / (float)_characterInfo.maxHealth;
            if (onHealthChanged != null)
                onHealthChanged(currentHealthPercent);
            if (_characterInfo.currentHealth <= 0)
            {
                Deactivate();
            }
        }

        protected void ModifyHealth(AttackInfo pAttackInfo)
        {
            _characterInfo.currentHealth -= pAttackInfo.damage;

            float currentHealthPercent = (float)_characterInfo.currentHealth / (float)_characterInfo.maxHealth;
            if (onHealthChanged != null)
                onHealthChanged(currentHealthPercent);
            if (_characterInfo.currentHealth <= 0)
            {
                // Show kill info 
                GameManager.instance.uIManager.killListPanel.AddItem(
                    new KillListItem.KillData(
                        pAttackInfo.attackerName,
                        characterInfo.name,
                        pAttackInfo.weaponSprite)
                    );
                Deactivate();
            }
        }

        public void GenerateId()
        {
            characterInfo.id = Guid.NewGuid().ToString("N");
        }

        public virtual void Deactivate()
        {
            Debug.Log("Character destroyed: " + _characterInfo.id);
            if (onCharacterDeactivate != null)
                onCharacterDeactivate(_characterInfo.tag, _characterInfo.id);
            GameManager.TriggerEvent("onCharacterKilled", new Dictionary<string, object> { { "id", characterInfo.id } });
            gameObject.SetActive(false);
            GameManager.instance.charactersHolder.characters.Remove(this);
        }

        public virtual void Respawn()
        {
            if (onCharacterRespawn != null)
                onCharacterRespawn(_characterInfo.tag, _characterInfo.id);
        }

        public virtual void Activate()
        {
            Debug.Log("Activate " + _characterInfo.id);
            //characterInfo = new CharacterInfo();
            _characterInfo.currentHealth = _characterInfo.maxHealth;
            gameObject.SetActive(true);
            GameManager.instance.charactersHolder.characters.Add(this);

            if (onHealthChanged != null)
                onHealthChanged(100f);
            if (onCharacterActivate != null)
                onCharacterActivate(_characterInfo.tag, _characterInfo.id);
        }

        public virtual void Damage(AttackInfo pAttackInfo)
        {
            Debug.Log("Damage : " + pAttackInfo.damage + " from " + pAttackInfo.attackerName);
            ModifyHealth(pAttackInfo);
        }
    }
}
