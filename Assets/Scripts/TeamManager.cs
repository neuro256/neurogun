﻿using Neurogun.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun
{
    [System.Serializable]
    public class Team
    {
        [System.Serializable]
        public struct TeammateData
        {
            public SpawnPoint _spawnPoint;
            public Character _player;
            public string p_layerName;
        }

        [SerializeField]
        private string _teamName;
        [SerializeField]
        private List<TeammateData> _playersList;
        [SerializeField]
        private Color _teamColor = Color.red;

        public string teamName { get => _teamName; set => _teamName = value; }
        public List<TeammateData> playersList { get => _playersList; set => _playersList = value; }
        public Color teamColor { get => _teamColor; set => _teamColor = value; }
        public int count
        {
            get
            {
                if (playersList == null)
                    return 0;
                return playersList.Count;
            }
        }

        public Team()
        {
            _playersList = new List<TeammateData>();
        }
    }

    public class TeamManager : MonoBehaviour
    {
        [SerializeField]
        private List<Team> _teams;

        public void Create()
        {
            // Неповторяющийся список spawnPoints
            HashSet<ISpawnObject> spawnPoints = new HashSet<ISpawnObject>();

            foreach (var team in _teams)
            {
                foreach (var teammate in team.playersList)
                {
                    CharacterInfo info = new CharacterInfo();
                    info.SetTeamData(team.teamName, team.teamColor);
                    info.SetName(teammate.p_layerName);
                    info.tag = teammate._player.characterInfo.tag;

                    teammate._spawnPoint.AddCharacter(info);

                    spawnPoints.Add(teammate._spawnPoint);
                }
            }
            foreach (var spawnPoint in spawnPoints)
            {
                spawnPoint.SpawnAll();
            }
        }
    }
}
