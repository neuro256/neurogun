﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neurogun.Interfaces
{
    public interface IParse
    {
        Dictionary<string, string> Parse(string pLanguage);
    }
}

