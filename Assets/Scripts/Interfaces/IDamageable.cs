﻿using Neurogun.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neurogun.Interfaces
{
    interface IDamageable
    {
        void Damage(AttackInfo pAttackInfo);
    }
}
