﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neurogun.Interfaces
{
    public interface ISpawnObject
    {
        void SpawnAll();
        void AddCharacter(CharacterInfo pCharacterInfo);
        void RemoveCharacter(CharacterInfo pCharacterInfo);
        IEnumerator Spawn();
        IEnumerator Respawn(string pTag, string pId);
    }
}

