﻿using Neurogun.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun.Weapons
{

    public class AssaultRifle : WeaponBase
    {
        private void Start()
        {
            currentClipCapacity = weaponData.clipCapacity;
        }

        private void Update()
        {
            Debug.DrawRay(firePosition.position, firePosition.forward * weaponData.shootingRange, Color.green);
        }

        public override void Fire(WeaponTriggerState triggerState)
        {
            if(triggerState == WeaponTriggerState.triggerPressed)
            {
                Fire();
            }
        }

        private void Fire()
        {
            if (!_enableFire)
                return;

            GameObject bulletObject = GameManager.instance.objectPooler.Spawn(weaponData.bulletPrefab.name, firePosition.position, Quaternion.identity);
            Bullet bullet = bulletObject.GetComponent<Bullet>();


            // Эффект выстрела
            muzzleFlash.Play();

            RaycastHit hit;

            if (Physics.Raycast(firePosition.position, firePosition.forward, out hit, weaponData.shootingRange))
            {
                IDamageable damagable = hit.collider.GetComponent<IDamageable>();

                if (damagable != null)
                {
                    AttackInfo attackInfo = new AttackInfo();
                    attackInfo.SetAttackerInfo(_characterInfo.id, _characterInfo.name);
                    attackInfo.SetWeaponInfo(weaponData.weaponId, weaponData.icon);
                    attackInfo.SetDamageInfo(weaponData.damage);
                    damagable.Damage(attackInfo);
                }

                if (hit.rigidbody != null)
                {
                    hit.rigidbody.AddForce(-hit.normal * weaponData.hitForce * weaponData.damage);
                }

                bullet.Shoot(firePosition, weaponData.bulletSpeed, hit.distance);
            }
            else
            {
                bullet.Shoot(firePosition, weaponData.bulletSpeed, weaponData.shootingRange);
            }

            _currentClipCapacity--;

            if (currentClipCapacity == 0) // Обойма пуста
            {
                // Перезарядка
                StartCoroutine(Reload());
            }
            else
            {
                // Задержка следующего выстрела
                StartCoroutine(ShotDelay());
            }
        }
    }
}
