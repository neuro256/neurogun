﻿using UnityEngine;

namespace Neurogun.Weapons
{

    [CreateAssetMenu(fileName = "New WeaponData", menuName = "Weapon Data", order = 51)]
    public class WeaponData : ScriptableObject
    {
        [SerializeField]
        private string _weaponId;
        [SerializeField]
        private string _weaponName;
        [SerializeField]
        private float _rateOfFire; // Скорострельность
        [SerializeField]
        private float _reloadTime; // Время перезарядки
        [SerializeField]
        private float _damage;
        [SerializeField]
        private float _shootingRange; // Дальность стрельбы
        [SerializeField]
        private int _numberOfBullets; // Количество создаваемых пуль за выстрел
        [SerializeField]
        private float _bulletSpreadRate; // Разброс пуль
        [SerializeField]
        private int _clipCapacity; // Емкость обоймы
        [SerializeField]
        private float _hitForce; // Отдача от выстрела
        [SerializeField]
        private float _bulletSpeed; // Скорость пули
        [SerializeField]
        private TypeOfShooting _typeOfShooting;
        [SerializeField]
        private Sprite _icon;
        [SerializeField]
        private GameObject _bulletPrefab;

        public string weaponId { get => _weaponId; }
        public string weaponName { get => _weaponName; }
        public float rateOfFire { get => _rateOfFire; }
        public float reloadTime { get => _reloadTime; }
        public float damage { get => _damage; }
        public int numberOfBullets { get => _numberOfBullets; }
        public int clipCapacity { get => _clipCapacity; }
        public TypeOfShooting typeOfShooting { get => _typeOfShooting; }
        public Sprite icon { get => _icon; }
        public float shootingRange { get => _shootingRange; }
        public GameObject bulletPrefab { get => _bulletPrefab; }
        public float bulletSpreadRate { get => _bulletSpreadRate;  }
        public float hitForce { get => _hitForce; }
        public float bulletSpeed { get => _bulletSpeed; }

        public enum TypeOfShooting
        {
            single = 0,
            semi_automatic = 1,
            automatic = 2,
            burst = 3
        }
    }
}