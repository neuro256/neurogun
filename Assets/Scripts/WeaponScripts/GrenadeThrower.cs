﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun.Weapons
{
    public class GrenadeThrower : MonoBehaviour
    {
        [SerializeField]
        [Range(100f, 500f)]
        private float _maxForce = 400f;
        [SerializeField]
        [Range(50f, 100f)]
        private float _minForce = 50f;
        [SerializeField]
        private GameObject _grenadePrefab;
        [SerializeField]
        private Transform _throwPosition;
        [SerializeField]
        private float _maxHoldTime = 3.0f;
        [SerializeField]
        private Character _character;
        [SerializeField]
        private DrawTrajectory _drawTrajectory;

        private float _currentForce;
        private float _currentHoldTime = 0f;
        private Rigidbody _rigibodyGrenade;
        private Vector3 _forceVector;
        

        // Start is called before the first frame update
        void Start()
        {
            _rigibodyGrenade = _grenadePrefab.GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void Update()
        {
            _forceVector = transform.forward * _currentForce + new Vector3(0f, _currentForce, 0f);
            // Отображаем отрисовку полета гранаты, пока удержана кнопка. Одновременно увеличиваем силу броска от минимальной до максимальной
            if (Input.GetMouseButton(2))
            {
                _currentHoldTime += Time.deltaTime;
                _currentForce = Mathf.Lerp(_minForce, _maxForce, _currentHoldTime / _maxHoldTime);
                _drawTrajectory.UpdateTrajectory(_forceVector, _rigibodyGrenade, _throwPosition.position);
            }
            else if (Input.GetMouseButtonUp(2))
            {
                ThrowGrenade();
                _drawTrajectory.HideLine();
                _currentForce = _minForce;
                _currentHoldTime = 0f;
            }
        }

        private void ThrowGrenade()
        {
            GameObject grenadeObj = Instantiate(_grenadePrefab, _throwPosition.position, _throwPosition.rotation);
            grenadeObj.GetComponent<Grenade>().character = _character;
            Rigidbody rb = grenadeObj.GetComponent<Rigidbody>();
            rb.AddForce(_forceVector);
            Debug.Log("Grenade force value: " + _forceVector);
        }
    }
}
