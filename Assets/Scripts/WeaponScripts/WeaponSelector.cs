﻿using System.Collections;
using UnityEngine;

namespace Neurogun.Weapons
{
    public class WeaponSelector : MonoBehaviour
    {
        [SerializeField]
        private WeaponBase _selectedWeapon;

        public WeaponBase selectedWeapon { get => _selectedWeapon; }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
