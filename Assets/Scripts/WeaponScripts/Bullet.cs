﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun.Weapons
{
    public class Bullet : MonoBehaviour
    {
        private float _shootingRange;
        private Vector3 _startPos;
        private Vector3 _direction;
        private bool _shooted = false;
        private float _force;
        // Start is called before the first frame update

        // Update is called once per frame
        void Update()
        {
            if (_shooted)
            {
                transform.position += _direction * _force * Time.deltaTime; 

                if ((_startPos - transform.position).magnitude > _shootingRange)
                {
                    Deactivate();
                }
            }
        }

        private void Deactivate()
        {
            gameObject.SetActive(false);
        }

        public void Shoot(Transform pFirePosition, float pForce, float pShootingRange)
        {
            transform.position = pFirePosition.position;
            _startPos = pFirePosition.position;
            _direction = pFirePosition.forward;
            _force = pForce;
            _shootingRange = pShootingRange;
            _shooted = true;
        }

        public void Shoot(Transform pFirePosition, float pForce, float pShootingRange, Vector3 pOffset)
        {
            transform.position = pFirePosition.position;
            _startPos = pFirePosition.position;
            _direction = pFirePosition.forward + pOffset;
            _force = pForce;
            _shootingRange = pShootingRange;
            _shooted = true;
        }
    }
}
