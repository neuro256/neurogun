﻿using Neurogun.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun.Weapons
{
    public class Grenade : MonoBehaviour
    {
        [SerializeField]
        private string _weaponId;
        [SerializeField]
        private float _delay = 3.0f;
        [SerializeField]
        private float _radius = 5f;
        [SerializeField]
        private float _force = 700f;
        [SerializeField]
        private float _damage = 80f;
        [SerializeField]
        private ParticleSystem _explosionPrefab;
        [SerializeField]
        private Sprite _grenadeSprite;

        private float _countdown;
        private bool _hasExploded = false;
        private Character _character;

        public Character character { get => _character; set => _character = value; }

        // Start is called before the first frame update
        void Start()
        {
            _countdown = _delay;
        }

        // Update is called once per frame
        void Update()
        {
            _countdown -= Time.deltaTime;

            if (_countdown <= 0 && !_hasExploded)
            {
                _hasExploded = true;
                Explode();
            }
        }

        private void Explode()
        {
            CheckDamage();

            // Эффект взрыва
            ExplosionEffect();

            Destroy(gameObject);
        }

        private void ExplosionEffect()
        {
            ParticleSystem fireExplosion = Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            fireExplosion.Play();
            Destroy(fireExplosion.gameObject, fireExplosion.main.duration);
        }

        private void CheckDamage()
        {
            // Игнорируем коллайдеры с указанной маской
            var ignoreMask = ~(LayerMask.GetMask("YuME"));
            // Получаем все доступные коллайдере в указанном радиусе
            Collider[] colliders = Physics.OverlapSphere(transform.position, _radius, ignoreMask);
            foreach (var nearbyCollider in colliders)
            {
                // Смотрим, стоит ли объект в прямой видимости от точки взрыва
                RaycastHit hit;
                if (Physics.Linecast(transform.position, nearbyCollider.transform.position, out hit))
                {
                    if (hit.collider == nearbyCollider)
                    {
                        Rigidbody rb = nearbyCollider.GetComponent<Rigidbody>();

                        if (rb != null)
                        {
                            Debug.Log("nearby: " + rb.gameObject);
                            rb.AddExplosionForce(_force, transform.position, _radius);
                        }

                        // Нанесение урона
                        IDamageable damagable = nearbyCollider.gameObject.GetComponent<IDamageable>();

                        AttackInfo attackInfo = new AttackInfo();
                        attackInfo.SetAttackerInfo(_character.characterInfo.id, _character.characterInfo.name);
                        attackInfo.SetWeaponInfo(_weaponId, _grenadeSprite);
                        attackInfo.SetDamageInfo(_damage);

                        if (damagable != null)
                            damagable.Damage(attackInfo);
                    }
                }
            }
        }
    }
}
