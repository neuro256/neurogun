﻿using Neurogun.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun.Weapons
{
    public class Shotgun : WeaponBase
    {
        private void Start()
        {
            currentClipCapacity = weaponData.clipCapacity;
        }

        private void Update()
        {
            Debug.DrawRay(firePosition.position, firePosition.forward * weaponData.shootingRange, Color.green);
        }

        public override void Fire(WeaponTriggerState triggerState)
        {
            if (triggerState == WeaponTriggerState.triggerDown && !_triggerPressed)
            {
                Fire();
                _triggerPressed = true;
            }
            else if (triggerState == WeaponTriggerState.triggerUp)
            {
                _triggerPressed = false;
            }
        }

        private void Fire()
        {
            if (!_enableFire)
                return;

            // Эффект выстрела
            muzzleFlash.Play();

            RaycastHit hit;
            Dictionary<IDamageable, int> hitDictionary = new Dictionary<IDamageable, int>();

            for (int i = 0; i < weaponData.numberOfBullets; i++)
            {
                GameObject bulletObject = GameManager.instance.objectPooler.Spawn(weaponData.bulletPrefab.name, firePosition.position, Quaternion.identity);
                Bullet bullet = bulletObject.GetComponent<Bullet>();

                // Вычисляем рандомный разброс дроби для выстрела из дробовика
                Vector3 _offset = transform.up * Random.Range(0.0f, weaponData.bulletSpreadRate);
                _offset = Quaternion.AngleAxis(Random.Range(0.0f, 360f), transform.forward) * _offset;

                if (Physics.Raycast(firePosition.position, firePosition.forward + _offset, out hit, weaponData.shootingRange))
                {
                    IDamageable damagable = hit.collider.GetComponent<IDamageable>();
                    // Считаем, кто сколько урона получил 
                    CalcHit(hitDictionary, damagable);

                    if (hit.rigidbody != null)
                    {
                        hit.rigidbody.AddForce(-hit.normal * weaponData.hitForce * (weaponData.damage / weaponData.numberOfBullets));
                    }

                    bullet.Shoot(firePosition, weaponData.bulletSpeed, hit.distance, _offset);
                }
                else
                {
                    bullet.Shoot(firePosition, weaponData.bulletSpeed, weaponData.shootingRange, _offset);
                }
            }

            // Наносим урон 
            HitEnemy(hitDictionary);

            _currentClipCapacity--;

            if (currentClipCapacity == 0) // Обойма пуста
            {
                // Перезарядка
                StartCoroutine(Reload());
            }
            else
            {
                // Задержка следующего выстрела
                StartCoroutine(ShotDelay());
            }
        }

        private void HitEnemy(Dictionary<IDamageable, int> hitDictionary)
        {
            foreach (var hitDamageable in hitDictionary)
            {
                AttackInfo attackInfo = new AttackInfo();
                attackInfo.SetAttackerInfo(_characterInfo.id, _characterInfo.name);
                attackInfo.SetWeaponInfo(weaponData.weaponId, weaponData.icon);
                float _damage = (float)(weaponData.damage * hitDamageable.Value) / weaponData.numberOfBullets;
                attackInfo.SetDamageInfo(_damage);

                hitDamageable.Key.Damage(attackInfo);
            }
        }

        private void CalcHit(Dictionary<IDamageable, int> hitDictionary, IDamageable damagable)
        {
            if (damagable != null)
            {
                int hitCount;
                if (hitDictionary.TryGetValue(damagable, out hitCount))
                {
                    hitDictionary[damagable] = hitCount + 1;
                }
                else
                {
                    hitDictionary[damagable] = 1;
                }
            }
        }
    }
}
