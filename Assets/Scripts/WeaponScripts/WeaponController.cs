﻿using UnityEngine;

namespace Neurogun.Weapons
{
    public class AttackInfo
    {
        private string _idAttacker;
        private string _attackerName;
        private string _idWeapon;
        private Sprite _weaponSprite;
        private float _damage;

        public float damage { get => _damage; }
        public string idWeapon { get => _idWeapon; }
        public string idAttacker { get => _idAttacker; }
        public string attackerName { get => _attackerName; }
        public Sprite weaponSprite { get => _weaponSprite; set => _weaponSprite = value; }

        public void SetAttackerInfo(string pIdAttacker, string pAttackerName)
        {
            this._idAttacker = pIdAttacker;
            this._attackerName = pAttackerName;
        }

        public void SetWeaponInfo(string pIdWeapon, Sprite pWeaponSprite)
        {
            this._idWeapon = pIdWeapon;
            this._weaponSprite = pWeaponSprite;
        }

        public void SetDamageInfo(float pDamage)
        {
            this._damage = pDamage;
        }
    }

    public class WeaponController : MonoBehaviour
    {
        [SerializeField]
        private Transform _firePosition;

        private WeaponBase _weapon;

        public Transform firePosition { get => _firePosition; }

        public void ResetWeapon()
        {
            _weapon?.ResetWeapon();
        }

        public void Fire(WeaponTriggerState triggerState)
        {
            _weapon?.Fire(triggerState);
        }

        public void SetWeapon(WeaponBase pWeapon)
        {
            if (_weapon != null)
                Destroy(_weapon.gameObject);
            _weapon = Instantiate(pWeapon, _firePosition);
            _weapon.firePosition = _firePosition;
        }

        public void SetWeapon(WeaponBase pWeapon, CharacterInfo pCharacterInfo)
        {
            if (_weapon != null)
                Destroy(_weapon.gameObject);
            _weapon = Instantiate(pWeapon, _firePosition);
            _weapon.firePosition = _firePosition;
            _weapon.characterInfo = pCharacterInfo;
        }
    }
}
