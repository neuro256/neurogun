﻿using Neurogun.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Neurogun.Weapons.WeaponData;

namespace Neurogun.Weapons
{
    public enum WeaponTriggerState
    {
        triggerDown = 0,
        triggerPressed = 1,
        triggerUp = 2
    }

    public abstract class WeaponBase : MonoBehaviour
    {
        [SerializeField]
        private WeaponData _weaponData;
        [SerializeField]
        private ParticleSystem _muzzleFlash;

        protected CharacterInfo _characterInfo;
        protected Transform _firePosition;
        protected bool _enableFire = true;
        protected bool _triggerPressed = false;
        protected int _currentClipCapacity; // Текущее количество патронов в обойме

        protected WeaponData weaponData { get => _weaponData; }
        protected int currentClipCapacity { get => _currentClipCapacity; set => _currentClipCapacity = value; }

        public Transform firePosition { get => _firePosition; set => _firePosition = value; }
        public CharacterInfo characterInfo { get => _characterInfo; set => _characterInfo = value; }
        public ParticleSystem muzzleFlash { get => _muzzleFlash; }

        protected IEnumerator ShotDelay()
        {
            _enableFire = false;
            yield return new WaitForSeconds(weaponData.rateOfFire);
            _enableFire = true;
        }

        protected IEnumerator Reload()
        {
            Debug.Log("Reload gun");
            _enableFire = false;
            GameManager.TriggerEvent("onReloadWeapon", new Dictionary<string, object> { { "id", characterInfo.id }, { "duration", weaponData.reloadTime} });
            yield return new WaitForSeconds(weaponData.reloadTime);
            _currentClipCapacity = weaponData.clipCapacity;
            _enableFire = true;
        }
        public void ResetWeapon()
        {
            Debug.Log("Reset weapon");
            StopAllCoroutines();
            _enableFire = true;
            _triggerPressed = false;
            _currentClipCapacity = weaponData.clipCapacity;
        }

        public abstract void Fire(WeaponTriggerState triggerState);
    }
}
