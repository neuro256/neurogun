﻿using Neurogun.Interfaces;
using UnityEngine;

namespace Neurogun
{
    public class Bot : Character, IPooledObject
    {
        void Awake()
        {          
            GenerateId();
        }
        public void OnObjectSpawn()
        {
            Activate();
        }
    }
}
