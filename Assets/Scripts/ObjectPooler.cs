﻿using Neurogun.Interfaces;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Neurogun
{
    public class ObjectPooler : MonoBehaviour
    {
        [System.Serializable]
        public class Pool
        {
            public string _tag;
            public GameObject _prefab;
            public int _size;
            public bool _shouldExpand;
        }

        [SerializeField]
        private List<Pool> _pools;

        private Dictionary<string, List<GameObject>> _poolDictionary;

        private void Awake()
        {
            StartPool();
        }

        private void StartPool()
        {
            _poolDictionary = new Dictionary<string, List<GameObject>>();

            foreach (Pool pool in _pools)
            {
                List<GameObject> objectPool = new List<GameObject>();

                for (int i = 0; i < pool._size; i++)
                {
                    GameObject obj = Instantiate(pool._prefab);
                    obj.SetActive(false);
                    obj.transform.parent = this.transform;
                    objectPool.Add(obj);
                }

                _poolDictionary.Add(pool._tag, objectPool);
            }
        }

        public GameObject Spawn(string pTag, Vector3 pPosition, Quaternion pRotation)
        {
            if (!_poolDictionary.ContainsKey(pTag))
            {
                Debug.Log("Not contain tag: " + pTag);
                return null;
            }

            GameObject objectToSpawn = null;
            foreach (var poolItem in _poolDictionary[pTag])
            {
                if (!poolItem.activeInHierarchy)
                {
                    objectToSpawn = poolItem;
                    if (objectToSpawn != null)
                    {
                        objectToSpawn.transform.position = pPosition;
                        objectToSpawn.transform.rotation = pRotation;
                        objectToSpawn.SetActive(true);

                        IPooledObject pooledObj = objectToSpawn.GetComponent<IPooledObject>();

                        if (pooledObj != null)
                        {
                            pooledObj.OnObjectSpawn();
                        }

                        return objectToSpawn;
                    }
                }
            }

            // Если в пуле не хватает объектов, то создаем еще один 
            Pool pool = _pools.First(item => item._tag == pTag);
            if (pool._shouldExpand)
            {
                GameObject obj = Instantiate(pool._prefab);

                obj.transform.SetParent(transform);
                obj.transform.position = pPosition;
                obj.transform.rotation = pRotation;
                obj.SetActive(true);

                _poolDictionary[pool._tag].Add(obj);

                IPooledObject pooledObj = obj.GetComponent<IPooledObject>();

                if (pooledObj != null)
                {
                    pooledObj.OnObjectSpawn();
                }

                return obj;
            }

            return objectToSpawn;
        }
    }
}
