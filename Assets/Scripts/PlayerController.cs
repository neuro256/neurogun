﻿using Neurogun.UI;
using Neurogun.Weapons;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField]
        private CharacterController _controller;
        [SerializeField]
        private WeaponController _weaponController;
        [SerializeField]
        private Player _player;
        [SerializeField]
        private Transform _lookAtCamera;
        [SerializeField]
        private float _speed = 5f;
        [SerializeField]
        private float _jumpForce = 3.0f;
        [SerializeField]
        private float _turnSmoothTime = 0.1f;

        private Cinemachine.CinemachineFreeLook _freeLook;
        private Camera _myCamera;
        private bool _groundedPlayer;
        private float _turnSmoothVelocity;
        private Vector3 _playerVelocity;
        private float _gravityValue = -9.81f;

        private void Start()
        {
            _myCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
            _freeLook = GameObject.FindWithTag("ThirdPersonCamera").GetComponent<Cinemachine.CinemachineFreeLook>();
            _freeLook.Follow = transform;
            _freeLook.LookAt = _lookAtCamera;
        }

        // Update is called once per frame
        void Update()
        {
            Move();

            Jump();

            ShowSetttings();

            Fire();
        }

        private void Fire()
        {
            if (Input.GetButtonDown("Fire1"))
            {
                _weaponController.Fire(WeaponTriggerState.triggerDown);
            }
            else if(Input.GetButton("Fire1"))
            {
                _weaponController.Fire(WeaponTriggerState.triggerPressed);
            }
            else if(Input.GetButtonUp("Fire1"))
            {
                _weaponController.Fire(WeaponTriggerState.triggerUp);
            }
        }

        private void ShowSetttings()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                GameManager.instance.uIManager.settingsUIWindow.Show();
            }
        }

        private void Move()
        {
            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");
            Vector3 direction = new Vector3(horizontal, 0.0f, vertical).normalized;

            if (direction.magnitude >= 0.1f)
            {
                // Сглаживание поворота персонажа
                float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + _myCamera.transform.eulerAngles.y;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref _turnSmoothVelocity, _turnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                // Движение персонажа относительно камеры
                Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                _controller.Move(moveDirection.normalized * _speed * Time.deltaTime);
            }
        }

        private void Jump()
        {
            _groundedPlayer = _controller.isGrounded;
            if (_groundedPlayer && _playerVelocity.y < 0)
            {
                _playerVelocity.y = 0f;
            }
            if (Input.GetButtonDown("Jump") && _groundedPlayer)
            {
                _playerVelocity.y += Mathf.Sqrt(-_jumpForce * _gravityValue);
            }

            _playerVelocity.y += _gravityValue * Time.deltaTime;

            _controller.Move(_playerVelocity * Time.deltaTime);
        }
        private void OnTriggerEnter(Collider pOther)
        {
            if (pOther != null)
            {
                Debug.Log("On trigger enter: " + pOther.gameObject);
                WeaponSelector selector = pOther.gameObject.GetComponent<WeaponSelector>();
                if (selector != null)
                    _weaponController.SetWeapon(selector.selectedWeapon, _player.characterInfo);
            }
        }
    }
}
