﻿using Neurogun.Interfaces;
using Neurogun.UI;
using System;
using System.Collections;
using UnityEngine;

namespace Neurogun
{
    public class Player : Character, IPooledObject
    {
        private void Awake()
        {          
            GenerateId();
        }
        // Use this for initialization
        void Start()
        {
            //PlayerTest();
            GameManager.instance.uIManager.respawnUIWindow.AddButtonOkClickListener(Respawn);
        }

        #region Test
        private void PlayerTest()
        {
            StartCoroutine(TestPlayerHealthAndDestroy());
        }

        private IEnumerator TestPlayerHealthAndDestroy()
        {
            while (true)
            {
                ModifyHealth(25f);

                yield return new WaitForSeconds(2f);
            }
        }

        #endregion

        public void OnObjectSpawn()
        {
            Activate();
        }

        public override void Deactivate()
        {
            base.Deactivate();
            GameManager.instance.uIManager.respawnUIWindow.Show();
        }
    }
}
