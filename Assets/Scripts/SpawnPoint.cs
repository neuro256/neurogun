﻿using Neurogun.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun
{
    public class SpawnPoint : MonoBehaviour, ISpawnObject
    {
        [SerializeField]
        private bool _enableRespawn = true;
        [SerializeField]
        private float _spawnDelay = 3f;
        [SerializeField]
        private float _respawnDelay = 5f;

        private List<CharacterInfo> _spawnCharacters = new List<CharacterInfo>();
        private Dictionary<string, string> _characterDictionary = new Dictionary<string, string>();

        public List<CharacterInfo> spawnCharacters { get => _spawnCharacters; set => _spawnCharacters = value; }

        private void Character_OnCharacterRespawn(string pTag, string pId)
        {
            StartCoroutine(Respawn(pTag, pId));
        }

        public void AddCharacter(CharacterInfo pCharacterInfo)
        {
            if (spawnCharacters == null)
                return;
            _spawnCharacters.Add(pCharacterInfo);
        }

        public void RemoveCharacter(CharacterInfo pCharacterInfo)
        {
            if (spawnCharacters == null)
                return;
            _spawnCharacters.Remove(pCharacterInfo);
        }

        public void SpawnAll()
        {
            StartCoroutine(Spawn());
        }

        public IEnumerator Spawn()
        {
            foreach (var spawnCharInfo in spawnCharacters)
            {
                GameObject characterObj = GameManager.instance.objectPooler.Spawn(spawnCharInfo.tag, transform.position, Quaternion.identity);
                Character character = characterObj.GetComponent<Character>();
                character.onCharacterDeactivate += CharacterDeactivate;
                character.onCharacterRespawn += Character_OnCharacterRespawn;

                character.characterInfo.SetTeamData(spawnCharInfo.teamName, spawnCharInfo.teamColor);
                character.characterInfo.SetName(spawnCharInfo.name);
                character.characterInfo.tag = spawnCharInfo.tag;

                _characterDictionary.Add(character.characterInfo.id, character.characterInfo.tag);

                yield return new WaitForSeconds(_spawnDelay);
            }
        }

        public void CharacterDeactivate(string pTag, string pId)
        {
            Debug.Log("SpawnPoint.OnCharacterDestroy: " + pId);
            _characterDictionary.Remove(pId);
            // respawn
            if (_enableRespawn)
                StartCoroutine(Respawn(pTag, pId));
        }

        public IEnumerator Respawn(string pTag, string pTd)
        {
            yield return new WaitForSeconds(_respawnDelay);

            GameObject characterObj = GameManager.instance.objectPooler.Spawn(pTag, transform.position, Quaternion.identity);
            Debug.Log("Respawn character on pos: " + characterObj.transform.position);
            Character character = characterObj.GetComponent<Character>();
            _characterDictionary.Add(character.characterInfo.id, character.characterInfo.tag);
        }
    }
}
