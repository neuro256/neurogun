﻿using Neurogun.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun.Localization
{
    public class LocalizationManager
    {
        private Dictionary<string, string> _localizationData = new Dictionary<string, string>();
        private AvailableLanguages _currentLanguage = AvailableLanguages.EN;

        public AvailableLanguages currentLanguage { get => _currentLanguage; }
        public enum AvailableLanguages
        {
            EN = 0,
            RU = 1
        }

        public LocalizationManager()
        {
            LoadLanguage(new LocalizationBaseJSON(), currentLanguage);
        }

        public string GetText(string pTextId)
        {
            if (!_localizationData.ContainsKey(pTextId))
            {
                Debug.Log("Cannot find text: " + pTextId);
                return pTextId;
            }
            return _localizationData[pTextId];
        }

        public void ChangeLanguage(AvailableLanguages pLanguage)
        {
            _currentLanguage = pLanguage;
            LoadLanguage(new LocalizationBaseJSON(), pLanguage);
        }

        public void LoadLanguage(IParse parser, AvailableLanguages language)
        {
            _localizationData = parser.Parse(language.ToString());
        }
    }
}
