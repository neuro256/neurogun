﻿using Neurogun.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Neurogun.Localization
{
    public class LocalizationBaseJSON : IParse
    {
        private string _path = "Data/localization";
        public Dictionary<string, string> Parse(string pLanguage)
        {
            Dictionary<string, string> localizationData = new Dictionary<string, string>();

            TextAsset textAsset = Resources.Load<TextAsset>(_path + "_" + pLanguage);

            string decodedString = textAsset.ToString();

            JSONObject jsonObject = new JSONObject(decodedString);


            for (int i = 0; i < jsonObject.Count; i++)
            {
                localizationData.Add(jsonObject.keys[i], jsonObject[i].str);
            }

            return localizationData;
        }
    }
}
