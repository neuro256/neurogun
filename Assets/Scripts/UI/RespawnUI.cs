﻿using Neurogun.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Neurogun.UI
{
    public class RespawnUI : BaseWindowUI
    {
        [SerializeField]
        private TextMeshProUGUI _title;
        [SerializeField]
        private TextMeshProUGUI _message;
        [SerializeField]
        private TextMeshProUGUI _buttonOkText;
        [SerializeField]
        private Button _buttonOk;

        public TextMeshProUGUI title { get => _title; set => _title = value; }
        public TextMeshProUGUI message { get => _message; set => _message = value; }
        public Button buttonOk { get => _buttonOk; set => _buttonOk = value; }

        private void Awake()
        {
            _dialog = new Dialog();
            buttonOk.onClick.RemoveAllListeners();
            buttonOk.onClick.AddListener(Hide);
        }

        private void SetDialogText()
        {
            SetTitle(GameManager.instance.localizationManager.GetText("RespawnWindow_title"));
            SetMessage(GameManager.instance.localizationManager.GetText("RespawnWindow_message"));
            SetButtonOkText(GameManager.instance.localizationManager.GetText("RespawnWindow_buttonOk"));
        }

        public void AddButtonOkClickListener(UnityAction pListener)
        {
            buttonOk.onClick.AddListener(pListener);
        }

        public override void Show()
        {
            base.Show();

            SetDialogText();

            title.text = _dialog.title;
            message.text = _dialog.message;
            _buttonOkText.text = _dialog.buttonOkText;

            _dialog = new Dialog();
        }

        public override void Hide()
        {
            base.Hide();
        }

        public void SetTitle(string pTitle)
        {
            _dialog.title = pTitle;
        }

        public void SetMessage(string pMessage)
        {
            _dialog.message = pMessage;
        }

        public void SetButtonOkText(string pBtnOkText)
        {
            _dialog.buttonOkText = pBtnOkText;
        }
    }
}
