﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Neurogun.UI
{
    public class KillListItem : MonoBehaviour
    {
        public struct KillData
        {
            private string _killerText;
            private string _murderedText;
            private Sprite _icon;

            public KillData(string pKillerText, string pMurderedText, Sprite pIcon)
            {
                this._killerText = pKillerText;
                this._murderedText = pMurderedText;
                this._icon = pIcon;
            }

            public string killerText { get => _killerText; set => _killerText = value; }
            public string murderedText { get => _murderedText; set => _murderedText = value; }
            public Sprite icon { get => _icon; set => _icon = value; }
        }

        [SerializeField]
        private TextMeshProUGUI _killerText;
        [SerializeField]
        private TextMeshProUGUI _murderedText;
        [SerializeField]
        private Image _icon;
        [SerializeField]
        private float _selfDestructTime = 3f;

        private KillData _itemData;

        public TextMeshProUGUI killerText { get => _killerText; set => _killerText = value; }
        public TextMeshProUGUI murderedText { get => _murderedText; set => _murderedText = value; }
        public Image icon { get => _icon; set => _icon = value; }
        public KillData itemData { get => _itemData; }


        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(SelfDestruct());
        }

        private IEnumerator SelfDestruct()
        {
            yield return new WaitForSeconds(_selfDestructTime);

            Destroy(gameObject);
        }

        public void SetItemData(string pKillerText, string pMurderedText, Sprite pIcon)
        {
            _itemData = new KillData();
            _itemData.killerText = pKillerText;
            _itemData.murderedText = pMurderedText;
            _itemData.icon = pIcon;

            this.killerText.text = _itemData.killerText;
            this.murderedText.text = _itemData.murderedText;
            this.icon.sprite = pIcon;
        }
    }
}
