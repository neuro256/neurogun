﻿using Neurogun.Localization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Neurogun.UI
{
    public class SettingsUI : BaseWindowUI
    {
        [SerializeField]
        private TextMeshProUGUI _title;
        [SerializeField]
        private TextMeshProUGUI _buttonOkText;
        [SerializeField]
        private Button _buttonOK;
        [SerializeField]
        private TMP_Dropdown _dropdown;

        private void Awake()
        {
            _dropdown.onValueChanged.RemoveAllListeners();
            _dropdown.onValueChanged.AddListener(delegate { OnSelectLanguage(_dropdown); });
            _buttonOK.onClick.RemoveAllListeners();
            _buttonOK.onClick.AddListener(Hide);
            _dialog = new Dialog();
        }

        private void SetDialogText()
        {
            _title.text = GameManager.instance.localizationManager.GetText("SettingWindow_title");
            _buttonOkText.text = GameManager.instance.localizationManager.GetText("SettingWindow_buttonOk");
        }

        private void OnSelectLanguage(TMP_Dropdown pDropdown)
        {
            int index = pDropdown.value;

            switch (pDropdown.options[index].text)
            {
                case "English":
                    GameManager.instance.localizationManager.ChangeLanguage(LocalizationManager.AvailableLanguages.EN);
                    break;
                case "Русский":
                    GameManager.instance.localizationManager.ChangeLanguage(LocalizationManager.AvailableLanguages.RU);
                    break;
            }

            SetDialogText();
        }

        public override void Show()
        {
            base.Show();

            SetDialogText();      
        }

        public override void Hide()
        {
            base.Hide();
        }
    }
}
