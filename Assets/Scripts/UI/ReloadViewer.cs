﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Neurogun.UI
{
    public class ReloadViewer : MonoBehaviour
    {
        [SerializeField]
        private Image _foregroundImage;
        [SerializeField]
        private TextMeshProUGUI _header;
        [SerializeField]
        private Character _character;

        private float _reloadDuration;
        private float remainingTime;

        private bool _enableStart = false;

        private void Start()
        {
            _foregroundImage.fillAmount = 100f;
            _foregroundImage.enabled = false;
            _header.enabled = false;
        }

        private void OnEnable()
        {
            GameManager.StartListening("onReloadWeapon", StartReloadView);
        }

        private void OnDisable()
        {
            GameManager.StopListening("onReloadWeapon", StartReloadView);
        }

        private void Update()
        {
            if(_enableStart && remainingTime > 0f)
            {
                remainingTime -= Time.deltaTime;
                _foregroundImage.fillAmount = remainingTime / _reloadDuration;
            }
            else if(remainingTime <= 0f)
            {
                _enableStart = false;
                _foregroundImage.enabled = false;
                _header.enabled = false;
            }
        }

        private void LateUpdate()
        {
            transform.LookAt(Camera.main.transform);
            transform.Rotate(0, 180, 0);
        }

        private void StartReloadView(Dictionary<string, object> pMessage)
        {
            string _id = (string)pMessage["id"];
            if (string.Equals(_id, _character.characterInfo.id))
            {
                _reloadDuration = (float)pMessage["duration"];
                remainingTime = _reloadDuration;
                _enableStart = true;
                _foregroundImage.fillAmount = 100f;
                _foregroundImage.enabled = true;
                _header.enabled = true;
                _header.text = GameManager.instance.localizationManager.GetText("Reload_gun");
            }
        }
    }
}
