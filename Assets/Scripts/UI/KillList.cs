﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun.UI
{
    public class KillList : MonoBehaviour
    {
        [SerializeField]
        private KillListItem _killListItem;

        public void AddItem(KillListItem.KillData pKillData)
        {
            KillListItem itemObject = Instantiate(_killListItem, transform);

            itemObject.SetItemData(pKillData.killerText, pKillData.murderedText, pKillData.icon);

            itemObject.transform.SetAsFirstSibling();
        }
    }
}
