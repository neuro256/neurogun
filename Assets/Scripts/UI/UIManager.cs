﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun.UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField]
        private RespawnUI _respawnUIWindow;
        [SerializeField]
        private SettingsUI _settingsUIWindow;
        [SerializeField]
        private KillList _killListPanel;

        private List<BaseWindowUI> _windowsList;


        public RespawnUI respawnUIWindow { get => _respawnUIWindow; }
        public SettingsUI settingsUIWindow { get => _settingsUIWindow; }
        public KillList killListPanel { get => _killListPanel; }


        private void OnEnable()
        {
            //HideCursor(null);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            _windowsList = new List<BaseWindowUI>();
            _windowsList.Add(_respawnUIWindow);
            _windowsList.Add(_settingsUIWindow);

            GameManager.StartListening("onShowWindow", ShowCursor);
            GameManager.StartListening("onHideWindow", HideCursor);
        }

        private void OnDisable()
        {
            GameManager.StopListening("onShowWindow", ShowCursor);
            GameManager.StopListening("onHideWindow", HideCursor);
        }

        private void HideCursor(Dictionary<string, object> obj)
        {
            Debug.Log("Hide cursor");
            foreach (var window in _windowsList)
            {
                if (window.isWindowShown)
                    return;
            }

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void ShowCursor(Dictionary<string, object> obj)
        {
            Debug.Log("ShowCursor");
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
