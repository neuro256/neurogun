﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Neurogun.UI
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField]
        private Character _character;
        [SerializeField]
        private Image _foregroundImage;

        // Start is called before the first frame update
        void Start()
        {
            _character.onHealthChanged += HealthBar_OnHealthChanged;
            _foregroundImage.color = _character.characterInfo.teamColor;
        }

        private void LateUpdate()
        {
            transform.LookAt(Camera.main.transform);
            transform.Rotate(0, 180, 0);
        }

        private void HealthBar_OnHealthChanged(float pPercent)
        {
            _foregroundImage.fillAmount = pPercent;
        }
    }
}
