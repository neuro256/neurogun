﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun.UI
{
    public class Dialog
    {
        private string _title;
        private string _message;
        private string _buttonOkText;

        public string title { get => _title; set => _title = value; }
        public string message { get => _message; set => _message = value; }
        public string buttonOkText { get => _buttonOkText; set => _buttonOkText = value; }
    }

    public abstract class BaseWindowUI : MonoBehaviour
    {
        [SerializeField]
        private GameObject _canvas;

        private bool _isWindowShown = false;

        protected Dialog _dialog;

        public GameObject canvas { get => _canvas; set => _canvas = value; }
        public bool isWindowShown { get => _isWindowShown; }

        public virtual void Show()
        {
            canvas.SetActive(true);
            _isWindowShown = true;
            GameManager.TriggerEvent("onShowWindow", null);

        }

        public virtual void Hide()
        {
            canvas.SetActive(false);
            _isWindowShown = false;
            GameManager.TriggerEvent("onHideWindow", null);
        }
    }
}
