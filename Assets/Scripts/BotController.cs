﻿using Neurogun.Weapons;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun
{
    public class BotController : MonoBehaviour
    {
        [SerializeField]
        WeaponBase _weapon;
        [SerializeField]
        Bot _bot;
        [SerializeField]
        private WeaponController _weaponController;
        [SerializeField]
        private float _detectionRadius = 15f;
        [SerializeField]
        private float _detectionAngle = 45f;
        [SerializeField]
        private float _attackAngle = 20f;
        [SerializeField]
        private float _rotationSpeed = 1.2f;
        [SerializeField]
        private float _moveSpeed = 0.8f;
        [SerializeField]
        private float _minDistance = 5.0f;

        // Start is called before the first frame update
        void Start()
        {
            _weaponController.SetWeapon(_weapon, _bot.characterInfo);
        }

        // Update is called once per frame
        void Update()
        {
            //Fire();
            EnemyDetection();
        }

        private void OnEnable()
        {
            GameManager.StartListening("onCharacterKilled", OnCharacterKilled);
        }


        private void OnDisable()
        {
            GameManager.StopListening("onCharacterKilled", OnCharacterKilled);
        }

        private void OnCharacterKilled(Dictionary<string, object> pMessage)
        {           
            string _id = (string) pMessage["id"];
            if (string.Equals(_id, _bot.characterInfo.id))
            {
                Debug.Log("Character " + _id + " killed and weapon reset");
                _weaponController?.ResetWeapon();
            }
        }

        private void EnemyDetection()
        {
            var ignoreMask = ~(LayerMask.GetMask("YuME"));
            Collider[] colliders = Physics.OverlapSphere(transform.position, _detectionRadius, ignoreMask);
            foreach (var nearbyCollider in colliders)
            {
                if (nearbyCollider.gameObject.CompareTag("Player"))
                {
                    Vector3 playerPosition = nearbyCollider.gameObject.transform.position;
                    Vector3 targetDir = playerPosition - transform.position;
                    float angleBetweenEnemy = Vector3.Angle(targetDir, transform.forward);
                    if (angleBetweenEnemy < _detectionAngle)
                    {
                        transform.rotation = Quaternion.Slerp(transform.rotation,
                            Quaternion.LookRotation(playerPosition - transform.position),
                            _rotationSpeed * Time.deltaTime);

                        if (angleBetweenEnemy < _attackAngle)
                        {
                            Fire();
                            float distance = Vector3.Distance(playerPosition, transform.position);
                            if (distance > _minDistance)
                            {
                                transform.position += transform.forward * _moveSpeed * Time.deltaTime;
                            }
                        }
                    }
                }
            }
        }

        private void Fire()
        {
            _weaponController?.Fire(WeaponTriggerState.triggerPressed);
        }
    }
}
