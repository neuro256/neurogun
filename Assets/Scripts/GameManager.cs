﻿using Neurogun.Localization;
using Neurogun.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Neurogun
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager _instance = null;

        [SerializeField]
        private ObjectPooler _objectPooler;
        [SerializeField]
        private UIManager _UIManager;
        [SerializeField]
        private TeamManager _teamManager;

        private LocalizationManager _localizationManager;
        private CharactersHolder _charactersHolder;
        private Dictionary<string, Action<Dictionary<string, object>>> _eventDictionary;

        public ObjectPooler objectPooler { get => _objectPooler; }
        public UIManager uIManager { get => _UIManager; }
        public TeamManager teamManager { get => _teamManager; }

        public CharactersHolder charactersHolder
        {
            get
            {
                if (_charactersHolder == null)
                    _charactersHolder = new CharactersHolder();
                return _charactersHolder;
            }
        }

        public LocalizationManager localizationManager
        {
            get
            {
                if (_localizationManager == null)
                    _localizationManager = new LocalizationManager();
                return _localizationManager;
            }
        }

        public static GameManager instance 
        {
            get 
            { 
                if(!_instance)
                {
                    _instance = FindObjectOfType(typeof(GameManager)) as GameManager;
                    if(!_instance)
                    {
                        Debug.LogError("Game Manager is null");
                    }
                    else
                    {
                        _instance.Init();
                    }
                }

                return _instance;
            }
        }

        private void Start()
        {
            _teamManager.Create();
        }

        private void Init()
        {
            Debug.Log("Game manager init");

            if(_eventDictionary == null)
                _eventDictionary = new Dictionary<string, Action<Dictionary<string, object>>>();
            if(_localizationManager == null)
                _localizationManager = new LocalizationManager();
            if(_charactersHolder == null)
                _charactersHolder = new CharactersHolder();          
        }

        public static void StartListening(string pEventName, Action<Dictionary<string, object>> pListener)
        {
            Action<Dictionary<string, object>> _thisEvent;
            if (instance._eventDictionary.TryGetValue(pEventName, out _thisEvent))
            {
                _thisEvent += pListener;
                instance._eventDictionary[pEventName] = _thisEvent;
            }
            else
            {
                _thisEvent += pListener;
                instance._eventDictionary.Add(pEventName, _thisEvent);
            }
        }

        public static void StopListening(string pEventName, Action<Dictionary<string, object>> pListener)
        {
            Action<Dictionary<string, object>> _thisEvent;
            if(instance._eventDictionary.TryGetValue(pEventName, out _thisEvent))
            {
                _thisEvent -= pListener;
                instance._eventDictionary[pEventName] = _thisEvent;
            }
        }

        public static void TriggerEvent(string pEventName, Dictionary<string, object> message)
        {
            Action<Dictionary<string, object>> _thisEvent = null;
            if(instance._eventDictionary.TryGetValue(pEventName, out _thisEvent))
            {
                _thisEvent.Invoke(message);
            }
        }

    }
}
